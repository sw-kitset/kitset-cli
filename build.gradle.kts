group = "org.swkitset"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.7.21"
}

repositories {
    mavenCentral()
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("libreadline") {
                includeDirs("/usr/include", "/usr/include/readline")
            }
        }
        binaries {
            executable("kitset") {
                entryPoint = "org.swKitset.kitsetCli.main"
            }
        }
    }

    sourceSets {
        val kotlinVer = "1.7.21"
        commonMain {
            dependencies {
                implementation(kotlin("stdlib", kotlinVer))
            }
        }
    }
}
