package org.swKitset.kitsetCli.file

internal expect class SourceFile(name: String, srcDir: String, destDir: String) : ProjectFile

internal fun sourceFile(name: String, srcDir: String, destDir: String, init: SourceFile.() -> Unit = {}): SourceFile {
    val result = SourceFile(name = name, srcDir = srcDir, destDir = destDir)
    result.init()
    return result
}
