package org.swKitset.kitsetCli.file

internal interface ProjectFile {
    val name: String
    val srcDir: String
    val destDir: String

    fun create()
}
