package org.swKitset.kitsetCli

internal expect val userConfDir: String
internal const val GEN_PROJECT_ACTION = "Generate Project"
internal const val GEN_PROJECT_HELP_ACTION = "Generate Project ?"
internal const val LIST_LANGUAGES_ACTION = "List Languages"
internal const val LIST_KITSETS_ACTION = "List Kitsets"
internal const val LIST_KITSETS_HELP_ACTION = "List Kitsets ?"
internal const val KITSET_USES_GRADLE_ACTION = "Kitset Uses Gradle"
internal const val KITSET_USES_GRADLE_HELP_ACTION = "Kitset Uses Gradle ?"
internal const val EXIT_ACTION = "Exit"
internal const val COMMAND_PROMPT_HELP_ACTION = "Help"
internal var scriptMode = false
internal var promptMode = false
internal expect val homeDir: String

internal expect fun generateProject(projectName: String, parentDir: String, lang: String = "kotlin", kitset: String)

internal fun printCommandPromptHelp() {
    val items = mutableListOf<String>()
    cliWords
        .filter { it.isCommand }
        .forEach { items += "  * ${it.name} (completion shortcut: ${it.completionShortcut})" }
    println(
        """
            -- Kitset Command Prompt Help --
            Below is a list of commands that can be used with the command prompt:
        """.trimIndent()
    )
    items.forEach { println(it) }
}

internal fun printProgramUsage() {
    println(
        """
            ---- Kitset Usage ----
            # Generate a project using a Kotlin based Kitset.
            kitset "$GEN_PROJECT_ACTION" kitset project_name parent_dir
              e.g. kitset "$GEN_PROJECT_ACTION" linuxX64 my_project /home/a_user/sw_projects
            # Generate a project using a Kitset for a specific language.
            kitset "$GEN_PROJECT_ACTION" language kitset project_name parent_dir
              e.g. kitset "$GEN_PROJECT_ACTION" kotlin linuxX64 my_project /home/a_user/sw_projects
            # Print registered languages.
            kitset "$LIST_LANGUAGES_ACTION"
            # Print Kitsets that are available for a language.
            kitset "$LIST_KITSETS_ACTION" lang
              e.g. kitset "$LIST_KITSETS_ACTION" kotlin
        """.trimIndent()
    )
}

internal fun printGenProjectCommandUsage() {
    println(
        """
           -- $GEN_PROJECT_ACTION Command Usage --
           * $GEN_PROJECT_ACTION language kitset project_name parent_dir
              e.g. $GEN_PROJECT_ACTION kotlin linuxX64 my_project /home/a_user/sw_projects
        """.trimIndent()
    )
}

internal fun printListKitsetsCommandUsage() {
    println(
        """
           -- $LIST_KITSETS_ACTION Command Usage --
           * $LIST_KITSETS_ACTION language
              e.g. $LIST_KITSETS_ACTION kotlin
        """.trimIndent()
    )
}

internal fun printKitsetUsesGradleCommandUsage() {
    println(
        """
           -- $KITSET_USES_GRADLE_ACTION Command Usage --
           * $KITSET_USES_GRADLE_ACTION language kitset
              e.g. $KITSET_USES_GRADLE_ACTION kotlin linuxX64
        """.trimIndent()
    )
}

internal expect fun listRegisteredLanguages(): Array<String>

internal expect fun listKitsets(lang: String): Array<String>

internal expect fun kitsetExists(lang: String, kitset: String): Boolean

internal expect fun createKitsetDirectories()

internal expect fun kitsetUsesGradle(lang: String, kitset: String): Boolean

internal expect fun printKitsetUsesGradle(lang: String, kitset: String)

internal expect fun checkLanguageExists(lang: String)

internal expect fun checkKitsetExists(lang: String, kitset: String)

internal fun printKitsets(lang: String) {
    checkLanguageExists(lang)
    if (!scriptMode && lang in listRegisteredLanguages()) {
        println("Kitsets using the $lang language:")
        listKitsets(lang).forEach { println("  * $it") }
    } else if (lang in listRegisteredLanguages()) {
        listKitsets(lang).forEach { println(it) }
    }
}

internal fun printRegisteredLanguages() {
    if (!scriptMode) {
        println("Registered languages:")
        listRegisteredLanguages().forEach { println("  * $it") }
    } else {
        listRegisteredLanguages().forEach { println(it) }
    }
}
