package org.swKitset.kitsetCli

const val CLI_READ_DELAY = 50u
const val RESET_OUTPUT = "\u001B[0m"
const val BOLD = "\u001B[1m"
const val RED_COLOR = "\u001B[31m"
const val CYAN_COLOR = "\u001B[36m"
const val BLUE_BG_COLOR = "\u001B[44m"

internal val cliWords = arrayOf(
    CliWord(name = GEN_PROJECT_ACTION, completionShortcut = "gen", isCommand = true),
    CliWord(name = GEN_PROJECT_HELP_ACTION, completionShortcut = "gen?", isCommand = true),
    CliWord(name = KITSET_USES_GRADLE_ACTION, "gradle", isCommand = true),
    CliWord(name = KITSET_USES_GRADLE_HELP_ACTION, completionShortcut = "gradle?", isCommand = true),
    CliWord(name = LIST_LANGUAGES_ACTION, completionShortcut = "languages", isCommand = true),
    CliWord(name = LIST_KITSETS_ACTION, completionShortcut = "kitsets", isCommand = true),
    CliWord(name = LIST_KITSETS_HELP_ACTION, completionShortcut = "kitsets?", isCommand = true),
    CliWord(name = EXIT_ACTION, completionShortcut = "e", isCommand = true),
    CliWord(name = COMMAND_PROMPT_HELP_ACTION, completionShortcut = "?", isCommand = true),
    CliWord(name = "kotlin", completionShortcut = "kt", isCommand = false),
    CliWord(name = homeDir, completionShortcut = "home", isCommand = false)
)

internal expect fun runCommandPrompt()
