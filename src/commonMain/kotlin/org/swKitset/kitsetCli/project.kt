package org.swKitset.kitsetCli

import org.swKitset.kitsetCli.file.ProjectFile

internal expect class Project(
    name: String,
    parentDir: String,
    desc: String = "",
    license: String = "",
    entryPoint: String = "",
    lang: String = "kotlin",
    type: String
) {
    val files: MutableList<ProjectFile>

    fun create()
}

internal fun project(
    name: String,
    parentDir: String,
    kitset: String,
    lang: String,
    init: Project.() -> Unit = {}
): Project {
    val result = Project(name = name, parentDir = parentDir, type = kitset, lang = lang)
    result.init()
    return result
}
