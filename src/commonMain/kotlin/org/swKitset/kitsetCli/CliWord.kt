package org.swKitset.kitsetCli

internal data class CliWord(val name: String, val completionShortcut: String, val isCommand: Boolean)
