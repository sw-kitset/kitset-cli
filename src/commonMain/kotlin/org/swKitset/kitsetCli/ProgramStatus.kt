package org.swKitset.kitsetCli

internal enum class ProgramStatus(val returnCode: Int) {
    SUCCESS(0),
    LANG_NOT_REGISTERED(-1),
    KITSET_MISSING(-2),
    PROJECT_GEN_FAILED(-3),
    INVALID_SCRIPT_ARGS(-4),
    MISSING_KITSET_INFO(-5)
}
