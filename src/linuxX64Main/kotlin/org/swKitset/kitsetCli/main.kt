@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.swKitset.kitsetCli

import kotlin.system.exitProcess

fun main(args: Array<String>) {
    createKitsetDirectories()
    if (args.isEmpty()) {
        runCommandPrompt()
    } else if (args.isNotEmpty() && "-s" in args) {
        processScriptingArgs(args)
    } else if (args.size == 3 && args.first() == KITSET_USES_GRADLE_ACTION) {
        printKitsetUsesGradle(args[1], args[2])
    } else if (args.size == 1 && args.first() == LIST_LANGUAGES_ACTION) {
        printRegisteredLanguages()
    } else if (args.size == 2 && args.first() == LIST_KITSETS_ACTION) {
        printKitsets(args[1])
    } else if (args.size == 4 && args.first() == GEN_PROJECT_ACTION) {
        processGenKotlinProjectArgs(type = args[1], name = args[2], parentDir = args[3])
    } else if (args.size == 5 && args.first() == GEN_PROJECT_ACTION) {
        processGenProjectArgs(args)
    } else {
        printProgramUsage()
    }
}

private fun processScriptingArgs(args: Array<String>) {
    scriptMode = true
    if (args.size == 2 && args.first() == LIST_LANGUAGES_ACTION) {
        printRegisteredLanguages()
    } else if (args.size == 3 && args.first() == LIST_KITSETS_ACTION) {
        printKitsets(args[1])
    } else if (args.size == 4 && args.first() == KITSET_USES_GRADLE_ACTION) {
        printKitsetUsesGradle(args[1], args[2])
    } else {
        exitProcess(ProgramStatus.INVALID_SCRIPT_ARGS.returnCode)
    }
}

private fun processGenKotlinProjectArgs(name: String, type: String, parentDir: String) {
    checkKitsetExists("kotlin", type)
    generateProject(projectName = name, kitset = type, parentDir = parentDir)
}

private fun processGenProjectArgs(args: Array<String>) {
    val lang = args[1]
    val type = args[2]
    val name = args[3]
    val parentDir = args[4]
    checkLanguageExists(lang)
    checkKitsetExists(lang, type)
    generateProject(lang = lang, kitset = type, projectName = name, parentDir = parentDir)
}
