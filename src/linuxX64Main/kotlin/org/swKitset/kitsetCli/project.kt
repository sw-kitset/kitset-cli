package org.swKitset.kitsetCli

import kotlinx.cinterop.*
import org.swKitset.kitsetCli.file.ProjectFile
import org.swKitset.kitsetCli.file.sourceFile
import platform.posix.*

@Suppress("CanBeParameter")
@ExperimentalUnsignedTypes
internal actual class Project actual constructor(
    val name: String,
    val parentDir: String,
    val desc: String,
    val license: String,
    val entryPoint: String,
    val lang: String,
    val type: String
) {
    actual val files: MutableList<ProjectFile> = mutableListOf()
    private val projectDir = "$userConfDir/kitsets/$lang/$type"

    init {
        readFileList().forEach { data ->
            val delimiter = ','
            val tmp = data.split(delimiter)
            val srcDir = if (tmp.first() == ".") projectDir else "$projectDir/${tmp.first()}"
            val destDir = if (srcDir == projectDir) "$parentDir/$name" else "$parentDir/$name/${tmp.first()}"
            val fileName = tmp.last()
            files += sourceFile(name = fileName, srcDir = srcDir, destDir = destDir)
        }
    }

    private fun doPostSetup() {
        val postSetupItems = readPostSetup()
        if (postSetupItems.isNotEmpty()) {
            println("Doing post setup...")
            chdir("$parentDir/$name")
            postSetupItems.forEach { item ->
                if (!item.trim().startsWith("#")) {
                    println("Executing \"$item\"...")
                    system(item)
                }
            }
            println("${CYAN_COLOR}Post setup complete.$RESET_OUTPUT")
        }
    }

    private fun doPreSetup() {
        val preSetupItems = readPreSetup()
        if (preSetupItems.isNotEmpty()) {
            println("Doing pre setup...")
            chdir("$parentDir/$name")
            preSetupItems.forEach { item ->
                if (!item.trim().startsWith("#")) {
                    println("Executing \"$item\"...")
                    system(item)
                }
            }
            println("${CYAN_COLOR}Pre setup complete.$RESET_OUTPUT")
        }
    }

    actual fun create() {
        doPreSetup()
        val rc = mkdir("$parentDir/$name", (S_IRWXU or S_IRWXG).toUInt())
        if (rc == EEXIST) throw IllegalStateException("Project directory already exists.")
        else if (rc != 0) throw IllegalStateException("Cannot create project directory.")
        println("Copying files to new project...")
        files.forEach { it.create() }
        doPostSetup()
    }

    private fun readFileList(): Array<String> = memScoped {
        println("Reading file list...")
        val readMode = "r"
        val path = "$projectDir/.kitset_info/file_list.csv"
        val file = fopen(path, readMode)
            ?: throw IllegalStateException("Cannot open file list ($path).")
        val tmp = mutableListOf<String>()
        val lineBuf = alloc<CPointerVar<ByteVar>>()
        val lineBufSize = alloc<ULongVar>()
        var bytesRead: Long
        do {
            bytesRead = getline(__stream = file, __lineptr = lineBuf.ptr, __n = lineBufSize.ptr)
            if (bytesRead > 0L) tmp += lineBuf.value?.toKString() ?: ""
        } while (bytesRead > 0L)
        fclose(file)
        return tmp.filter { it != "\n" }.map { it.replace("\n", "") }.toTypedArray()
    }

    private fun readPostSetup(): Array<String> = memScoped {
        val readMode = "r"
        val file = fopen("$projectDir/.kitset_info/post_setup.txt", readMode)
        val tmp = mutableListOf<String>()
        val lineBuf = alloc<CPointerVar<ByteVar>>()
        val lineBufSize = alloc<ULongVar>()
        var bytesRead: Long

        if (file != null) {
            println("Reading post setup file...")
            do {
                bytesRead = getline(__stream = file, __lineptr = lineBuf.ptr, __n = lineBufSize.ptr)
                if (bytesRead > 0L) tmp += lineBuf.value?.toKString() ?: ""
            } while (bytesRead > 0L)
            fclose(file)
        }
        return tmp.filter { it != "\n" }.map { it.replace("\n", "") }.toTypedArray()
    }

    private fun readPreSetup(): Array<String> = memScoped {
        val readMode = "r"
        val file = fopen("$projectDir/.kitset_info/pre_setup.txt", readMode)
        val tmp = mutableListOf<String>()
        val lineBuf = alloc<CPointerVar<ByteVar>>()
        val lineBufSize = alloc<ULongVar>()
        var bytesRead: Long

        if (file != null) {
            println("Reading pre setup file...")
            do {
                bytesRead = getline(__stream = file, __lineptr = lineBuf.ptr, __n = lineBufSize.ptr)
                if (bytesRead > 0L) tmp += lineBuf.value?.toKString() ?: ""
            } while (bytesRead > 0L)
            fclose(file)
        }
        return tmp.filter { it != "\n" }.map { it.replace("\n", "") }.toTypedArray()
    }
}
