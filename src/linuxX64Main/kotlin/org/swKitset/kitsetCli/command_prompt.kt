@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS")

package org.swKitset.kitsetCli

import kotlinx.cinterop.*
import libreadline.*
import platform.posix.usleep

internal actual fun runCommandPrompt() {
    var line: String
    setupCommandPrompt()
    do {
        line = readline("Kitset > ")?.toKString()?.trim() ?: ""
        processLine(line)
        usleep(CLI_READ_DELAY)
    } while (line != EXIT_ACTION)
}

private fun processLine(line: String) {
    processHelpActions(line)
    processGeneralActions(line)
    processGeneralActionsWithArguments(line)
}

private fun readLine(prompt: String): String {
    var line: String
    do {
        line = readline(prompt)?.toKString()?.trim() ?: ""
        usleep(CLI_READ_DELAY)
    } while (line.isEmpty())
    add_history(line)
    return line
}

private fun processGeneralActions(line: String) {
    val totalArgs = line.split(" ").size
    when (line) {
        LIST_LANGUAGES_ACTION -> {
            add_history(line)
            printRegisteredLanguages()
        }
        LIST_KITSETS_ACTION -> {
            if (totalArgs == LIST_KITSETS_ACTION.split(" ").size) {
                add_history(line)
                printKitsets(readLine("${BOLD}Language: $RESET_OUTPUT"))
            }
        }
        KITSET_USES_GRADLE_ACTION -> {
            if (totalArgs == KITSET_USES_GRADLE_ACTION.split(" ").size) {
                add_history(line)
                printKitsetUsesGradle(
                    readLine("${BOLD}Language: $RESET_OUTPUT"),
                    readLine("${BOLD}Kitset: $RESET_OUTPUT")
                )
            }
        }
        GEN_PROJECT_ACTION -> {
            if (totalArgs == GEN_PROJECT_ACTION.split(" ").size) {
                add_history(line)
                generateProject(
                    lang = readLine("${BOLD}Language: $RESET_OUTPUT"),
                    kitset = readLine("${BOLD}Kitset: $RESET_OUTPUT"),
                    projectName = readLine("${BOLD}Project Name: $RESET_OUTPUT"),
                    parentDir = readLine("${BOLD}Parent Directory: $RESET_OUTPUT")
                )
            }
        }
    }
}

private fun processGeneralActionsWithArguments(line: String) {
    val args = line.split(" ")
    when {
        line.startsWith(GEN_PROJECT_ACTION) && args.size == 6 -> {
            add_history(line)
            generateProject(lang = args[2], kitset = args[3], projectName = args[4], parentDir = args[5])
        }
        line.startsWith(LIST_KITSETS_ACTION) && args.size == 3 && args.last() != "?" -> {
            add_history(line)
            printKitsets(args[2])
        }
        line.startsWith(KITSET_USES_GRADLE_ACTION) && args.size == 5 -> {
            add_history(line)
            printKitsetUsesGradle(args[3], args[4])
        }
    }
}

private fun processHelpActions(line: String) {
    when (line) {
        GEN_PROJECT_HELP_ACTION -> {
            add_history(line)
            printGenProjectCommandUsage()
        }
        LIST_KITSETS_HELP_ACTION -> {
            add_history(line)
            printListKitsetsCommandUsage()
        }
        COMMAND_PROMPT_HELP_ACTION -> {
            add_history(line)
            printCommandPromptHelp()
        }
        KITSET_USES_GRADLE_HELP_ACTION -> {
            add_history(line)
            printKitsetUsesGradleCommandUsage()
        }
    }
}

private fun setupCommandPrompt() {
    history_max_entries = 15
    promptMode = true
    rl_attempted_completion_function = staticCFunction(::completionCompleter)
}

private fun completionCompleter(
    txt: CPointer<ByteVar>?,
    @Suppress("UNUSED_PARAMETER") start: Int,
    @Suppress("UNUSED_PARAMETER") end: Int
): CPointer<CPointerVar<ByteVar>>? {
    rl_attempted_completion_over = true.intValue
    return rl_completion_matches(txt, staticCFunction(::completionMatches))
}

private fun completionMatches(txt: CPointer<ByteVar>?, state: Int): CPointer<ByteVar>? {
    val tmp = txt?.toKString() ?: ""
    var result = ""
    if (state == 0) {
        val cmd = cliWords.find { it.completionShortcut == tmp }
        if (cmd != null) result = cmd.name
    }
    // Duplicate Kotlin String otherwise a segmentation fault WILL occur.
    return if (result.isNotEmpty()) strdup(result) else null
}
