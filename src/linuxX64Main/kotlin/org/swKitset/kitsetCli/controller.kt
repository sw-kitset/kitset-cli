@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.swKitset.kitsetCli

import kotlinx.cinterop.*
import platform.posix.*
import kotlin.system.exitProcess

internal actual val homeDir = getenv("HOME")?.toKString() ?: ""
internal actual val userConfDir = "$homeDir/.kitset"

internal actual fun generateProject(projectName: String, parentDir: String, lang: String, kitset: String) {
    println("Generating a $kitset project that uses the $lang language...")
    try {
        project(name = projectName, parentDir = parentDir, kitset = kitset, lang = lang).create()
        println("${CYAN_COLOR}Project generated.$RESET_OUTPUT")
    } catch (ex: IllegalStateException) {
        fprintf(stderr, "$BLUE_BG_COLOR${RED_COLOR}Failed to generate project: ${ex.message}$RESET_OUTPUT\n")
        if (!promptMode) exitProcess(ProgramStatus.PROJECT_GEN_FAILED.returnCode)
    }
}

internal actual fun listKitsets(lang: String): Array<String> {
    val tmp = mutableListOf<String>()
    val dir = opendir("$userConfDir/kitsets/$lang")
    var entry: CPointer<dirent>?
    do {
        entry = readdir(dir)
        if (entry != null && entry.pointed.d_type.toInt() == DT_DIR) {
            val name = entry.pointed.d_name.toKString()
            if (name !in arrayOf(".", "..")) tmp += name
        }
    } while (entry != null)
    closedir(dir)
    return tmp.filter { !it.startsWith('.') }.toTypedArray()
}

internal actual fun listRegisteredLanguages(): Array<String> {
    val tmp = mutableListOf<String>()
    val dir = opendir("$userConfDir/kitsets")
    var entry: CPointer<dirent>?
    do {
        entry = readdir(dir)
        if (entry != null && entry.pointed.d_type.toInt() == DT_DIR) {
            val name = entry.pointed.d_name.toKString()
            if (name !in arrayOf(".", "..")) tmp += name
        }
    } while (entry != null)
    closedir(dir)
    return tmp.filter { !it.startsWith('.') }.toTypedArray()
}

internal actual fun kitsetExists(lang: String, kitset: String): Boolean {
    val dir = opendir("$userConfDir/kitsets/$lang/$kitset")
    return if (dir == null) {
        false
    } else {
        closedir(dir)
        true
    }
}

internal actual fun createKitsetDirectories() {
    val permissions = (S_IRWXU or S_IRWXG).toUInt()
    mkdir(userConfDir, permissions)
    mkdir("$userConfDir/kitsets", permissions)
    mkdir("$userConfDir/kitsets/kotlin", permissions)
}

internal fun createMultipleDirectories(path: String, permissions: UInt) {
    val pathDelimiter = '/'
    path.trimEnd(pathDelimiter).split(pathDelimiter).reduce { acc, str ->
        val newAcc = "$acc$pathDelimiter$str"
        mkdir(newAcc, permissions)
        newAcc
    }
}

internal actual fun printKitsetUsesGradle(lang: String, kitset: String) {
    checkLanguageExists(lang)
    checkKitsetExists(lang, kitset)
    try {
        if (!scriptMode) println("Kitset Uses Gradle: ${kitsetUsesGradle(lang, kitset)}")
        else println(kitsetUsesGradle(lang, kitset))
    } catch (ex: IllegalStateException) {
        fprintf(stderr, "$BLUE_BG_COLOR${RED_COLOR}Missing Kitset info: ${ex.message}$RESET_OUTPUT\n")
        if (!promptMode) exitProcess(ProgramStatus.MISSING_KITSET_INFO.returnCode)
    }
}

internal actual fun kitsetUsesGradle(lang: String, kitset: String): Boolean = memScoped {
    var result = false
    val readMode = "r"
    val lineBuf = alloc<CPointerVar<ByteVar>>()
    val lineBufSize = alloc<ULongVar>()
    val path = "$userConfDir/kitsets/$lang/$kitset/.kitset_info/meta.properties"
    val file = fopen(path, readMode)
        ?: throw IllegalStateException("The meta.properties file doesn't exist in the Kitset.")
    var bytesRead: Long
    do {
        bytesRead = getline(__stream = file, __lineptr = lineBuf.ptr, __n = lineBufSize.ptr)
        if (bytesRead > 0L) {
            val line = (lineBuf.value?.toKString() ?: "").replace("\n", "").trim()
            if (line.startsWith("usesGradle = ")) {
                @Suppress("ReplaceRangeToWithUntil")
                result = line.slice(line.indexOf('=') + 2..line.length - 1).toBoolean()
                break
            }
        }
    } while (bytesRead > 0L)
    fclose(file)
    result
}

internal val Boolean.intValue
    get() = if (this) 1 else 0

internal actual fun checkLanguageExists(lang: String) {
    if (lang !in listRegisteredLanguages()) {
        if (!scriptMode) {
            fprintf(stderr, "$BLUE_BG_COLOR${RED_COLOR}Language $lang isn't registered.$RESET_OUTPUT\n")
            printRegisteredLanguages()
        }
        if (!promptMode) exitProcess(ProgramStatus.LANG_NOT_REGISTERED.returnCode)
    }
}

internal actual fun checkKitsetExists(lang: String, kitset: String) {
    if (!kitsetExists(lang, kitset)) {
        fprintf(
            stderr, "$BLUE_BG_COLOR${RED_COLOR}Kitset $kitset doesn't exist for the $lang language." +
                "$RESET_OUTPUT\n"
        )
        if (!promptMode) exitProcess(ProgramStatus.KITSET_MISSING.returnCode)
    }
}
