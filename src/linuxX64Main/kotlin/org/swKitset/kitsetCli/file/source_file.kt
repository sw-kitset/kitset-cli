package org.swKitset.kitsetCli.file

import kotlinx.cinterop.*
import org.swKitset.kitsetCli.createMultipleDirectories
import platform.posix.*

@ExperimentalUnsignedTypes
internal actual class SourceFile actual constructor(
    override val name: String,
    override val srcDir: String,
    override val destDir: String
) : ProjectFile {
    @Suppress("UNREACHABLE_CODE")
    override fun create() = memScoped {
        // The following code is based on this code snippet: https://stackoverflow.com/a/7464280
        val permissions = (S_IRWXU or S_IRWXG).toUInt()
        val readError = -1L
        val openError = -1
        val inFd = open("$srcDir/$name", O_RDONLY)
        if (inFd == openError) throw IllegalStateException("Cannot open input file ($srcDir/$name).")
        // Inform Linux kernel to optimize file I/O for sequential reading.
        posix_fadvise(__fd = inFd, __offset = 0, __len = 0, __advise = POSIX_FADV_SEQUENTIAL)
        createMultipleDirectories(destDir, (S_IRWXU or S_IRWXG).toUInt())
        val outFd = open("$destDir/$name", O_WRONLY or O_CREAT, permissions)
        if (outFd == openError) throw IllegalStateException("Cannot open output file ($destDir/$name).")
        val bufSize = 8192
        val buf = allocArray<ByteVar>(8192)
        var readResult: Long

        do {
            readResult = read(__fd = inFd, __buf = buf, __nbytes = bufSize.toULong())
            if (readResult == readError) throw IllegalStateException("Cannot copy source file (read error).")
            val writeResult = write(__fd = outFd, __buf = buf.toKString().cstr, __n = readResult.toULong())
            if (writeResult != readResult) throw IllegalStateException("Cannot copy source file (write error).")
        } while (readResult > 0)
        close(inFd)
        close(outFd)
        Unit
    }

    override fun toString(): String = "[File: Name - $name, Src Dir - $srcDir, Dest Dir - $destDir]"
}
